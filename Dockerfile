FROM python:3.8

# Add requirements.txt
ADD requirements.txt /

# Install jupyter lab
RUN pip install -r requirements.txt

# Install Rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Add rust paths
ENV PATH="/root/.cargo/bin:${PATH}"

# ADD CMAKE which is needed for some rust packages
RUN apt-get update && apt-get install -y cmake

# Install rust packages
RUN cargo install evcxr_jupyter

# Install Rust ipython kernel
RUN evcxr_jupyter --install

CMD [ "--allow-root",  "--notebook-dir",  "\"/data\"", "--ip", "\"0.0.0.0\"" ]
ENTRYPOINT [ "jupyter-notebook"  ]

VOLUME [ "/data" ]
EXPOSE 8888/tcp