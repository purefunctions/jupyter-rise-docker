docker run --name jupyter-rise-notebook --rm -it -p 8888:8888 -p 8000:8000 -v %1:/data purefunctions/jupyter-rise-notebook --allow-root --ip='0.0.0.0'
